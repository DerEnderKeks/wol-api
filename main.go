package main

import (
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var port string
var repeatCount int
var sourceInterfaces []string

var macRe = regexp.MustCompile(`^([\da-fA-F]{2}[:-]){5}([\da-fA-F]{2})$`)

func init() {
	log.SetFlags(log.LstdFlags | log.LUTC)
	var err error

	port = os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if p, err := strconv.Atoi(port); err != nil {
		log.Fatalf("failed to parse PORT: %v", err)
	} else if p < 0 || p > 65535 {
		log.Fatalln("PORT must be within 0 to 65535")
	}

	repeat := os.Getenv("REPEAT")
	repeatCount = 3
	if repeat != "" {
		repeatCount, err = strconv.Atoi(repeat)
		if err != nil {
			log.Fatalf("failed to parse REPEAT: %v", err)
		}
		if repeatCount < 1 {
			log.Fatalln("REPEAT must be greater than 0")
		}
	}

	bcInterfaces := os.Getenv("INTERFACES")
	if bcInterfaces != "" {
		sourceInterfaces = strings.Split(bcInterfaces, ",")
	}
}

func main() {
	http.HandleFunc("/", wolHandler)
	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "OK", http.StatusOK)
	})

	log.Printf("starting web server on port %s", port)
	log.Fatalln(http.ListenAndServe(":"+port, nil))
}

func wolHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	mac := r.URL.Path[1:]

	if !macRe.Match([]byte(mac)) {
		http.Error(w, "invalid MAC address", http.StatusBadRequest)
		return
	}

	log.Printf("waking %s, requested by %s", mac, r.RemoteAddr)

	if err := sendAllWoLPackets(mac, sourceInterfaces); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
		return
	}

	http.Error(w, "magic packet sent successfully", http.StatusOK)
}
