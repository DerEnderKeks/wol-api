package main

import (
	"fmt"
	"github.com/mdlayher/ethernet"
	"github.com/mdlayher/packet"
	"github.com/sabhiram/go-wol/wol"
	"net"
)

const wolType ethernet.EtherType = 0x0842

func sendAllWoLPackets(mac string, sourceNames []string) error {
	var sources []net.Interface
	if len(sourceNames) != 0 {
		interfaces, err := namesToInterfaces(sourceNames)
		if err != nil {
			return fmt.Errorf("failed to get interfaces: %w", err)
		}
		sources = append(sources, interfaces...)
	} else {
		var err error
		sources, err = allInterfaces()
		if err != nil {
			return fmt.Errorf("failed to get all interfaces: %w", err)
		}
	}

	for _, source := range sources {
		err := sendWoLPacket(source, mac, repeatCount)
		if err != nil {
			return err
		}
	}
	return nil
}

func sendWoLPacket(source net.Interface, destMAC string, count int) error {
	magicPacket, err := wol.New(destMAC)
	if err != nil {
		return fmt.Errorf("failed to create magic packet: %w", err)
	}

	bs, err := magicPacket.Marshal()
	if err != nil {
		return fmt.Errorf("failed to marshal magic packet: %w", err)
	}

	etherFrame := &ethernet.Frame{
		Destination: ethernet.Broadcast,
		Source:      source.HardwareAddr,
		EtherType:   wolType,
		Payload:     bs,
	}

	c, err := packet.Listen(&source, packet.Raw, int(etherFrame.EtherType), nil)
	if err != nil {
		return fmt.Errorf("failed to listen on interface %s: %w", source.Name, err)
	}
	defer c.Close()

	b, err := etherFrame.MarshalBinary()
	if err != nil {
		return fmt.Errorf("failed to marshal ethernet frame: %w", err)
	}

	addr := &packet.Addr{HardwareAddr: etherFrame.Destination}
	for i := 0; i < count; i++ {
		n, err := c.WriteTo(b, addr)
		if err != nil {
			return fmt.Errorf("failed to write magic packet: %w", err)
		} else if n != len(b) {
			return fmt.Errorf("failed to write magic packet: only %d of %d bytes were sent", n, len(b))
		}
	}

	return nil
}
