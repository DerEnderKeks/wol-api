package main

import (
	"fmt"
	"net"
)

// allInterfaces returns all non-loopback interfaces that are up
func allInterfaces() ([]net.Interface, error) {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		return nil, fmt.Errorf("failed to get interfaces: %w", err)
	}

	var interfaces []net.Interface
	for _, i := range netInterfaces {
		if i.Flags&net.FlagLoopback != 0 || i.Flags&net.FlagUp == 0 {
			continue
		}

		interfaces = append(interfaces, i)
	}

	if len(interfaces) == 0 {
		return nil, fmt.Errorf("no valid interfaces found")
	}
	return interfaces, nil
}

func namesToInterfaces(names []string) ([]net.Interface, error) {
	var interfaces []net.Interface
	for _, name := range names {
		iface, err := net.InterfaceByName(name)
		if err != nil {
			return nil, fmt.Errorf("failed to get interface %s: %w", name, err)
		}
		interfaces = append(interfaces, *iface)
	}
	return interfaces, nil
}
