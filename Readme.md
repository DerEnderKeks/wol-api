# WoL API

This is a very simple API server to send WoL packets.
After receiving an appropriate POST request, the server will broadcast a WoL packet directly on layer 2 (EtherType `0x0842`, no IPv4 required). 

By default, WoL packets will be sent on all non-loopback interfaces.

## Installation

This server only works on Linux. Configuration is done through environment variables.

#### Docker

```shell
docker run --net=host -e PORT=8080 registry.gitlab.com/derenderkeks/wol-api:latest
```

#### Bare

Requires Go to be [installed](https://go.dev/doc/install).

```shell
go install gitlab.com/derenderkeks/wol-api@main
setcap CAP_NET_RAW+ep $(which wol-api)
PORT=8080 wol-api
```

### Configuration

The following environment variables are supported:

| Variable     | Default | Description                                                                           |
|--------------|---------|---------------------------------------------------------------------------------------|
| `PORT`       | `8080`  | Port to listen on                                                                     |
| `INTERFACES` |         | Comma-separated list of interfaces to send WoL packets on. Will use all when not set. |
| `REPEAT`     | `3`     | Number of WoL Packets to send per request                                             |

## Usage

Simply send a POST request to `/<mac>`.

```shell
curl -X POST http://localhost:8080/00:11:22:33:44:55
```

You can check if the server is running by sending a GET request to `/health`.

## License

See [License](License).
