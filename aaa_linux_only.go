//go:build !linux

package main

import (
	"fmt"
	"os"
)

func init() {
	// this runs first, failing early if the OS is not Linux
	// would fail to send packets anyway, since the packet lib only supports linux
	fmt.Println("incompatible operating system, only Linux is supported")
	os.Exit(1)
}
